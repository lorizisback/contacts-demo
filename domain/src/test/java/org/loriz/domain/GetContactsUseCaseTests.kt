package org.loriz.domain

import io.mockk.every
import io.mockk.mockk
import io.reactivex.Flowable
import org.junit.Test
import org.loriz.data.entities.Contact
import org.loriz.data.repository.ContactRepository
import org.loriz.domain.usecases.GetContactsUseCase
import org.loriz.domain.usecases.GetContactsUseCaseImpl

class GetContactsUseCaseTests {

    private val mockContactList = listOf(
        Contact(
            id = 1,
            displayName = "Marco Loriga",
            emails = hashSetOf("marco.loriga.ca@gmail.com"),
            phoneNumbers = hashSetOf("3773240338"),
            thumbnail = "http://www.fakethumb.org",
            photo = "http://www.fake.org"
        ), Contact(
            id = 2,
            displayName = "Nenno Lello",
            emails = hashSetOf("nenno.lello.ca@gmail.com"),
            phoneNumbers = hashSetOf("34532453"),
            thumbnail = "http://www.fakethumb.org",
            photo = "http://www.fake.org"
        ), Contact(
            id = 3,
            displayName = "Richard Benson",
            emails = hashSetOf("richard.benson.ca@gmail.com"),
            phoneNumbers = hashSetOf("12345753"),
            thumbnail = "http://www.fakethumb.org",
            photo = "http://www.fake.org"
        ), Contact(
            id = 4,
            displayName = "Max Pezzali",
            emails = hashSetOf("max.pezzali.ca@gmail.com"),
            phoneNumbers = hashSetOf("367356753"),
            thumbnail = "http://www.fakethumb.org",
            photo = "http://www.fake.org"
        )
    )

    private val mockRepository = mockk<ContactRepository>()

    private val useCase: GetContactsUseCase = GetContactsUseCaseImpl(mockRepository)


    @Test
    fun `getContacts with flowable of various contacts as input`() {
        every { mockRepository.contacts() } returns Flowable.just(mockContactList)

        useCase.getContacts().test()
            .assertValue { it == mockContactList.sorted() }
            .assertNoErrors()
            .assertComplete()
    }

    @Test
    fun `getContacts with flowable of one contact as input`() {
        every { mockRepository.contacts() } returns Flowable.just(mockContactList.subList(0,1))

        useCase.getContacts().test()
            .assertValue { it == listOf(mockContactList.first()) }
            .assertNoErrors()
            .assertComplete()
    }

    @Test
    fun `getContactById with flowable of various contacts as input and id present`() {
        every { mockRepository.contacts() } returns Flowable.just(mockContactList)

        useCase.getContactById(mockContactList.first().id).test()
            .assertValue { it == mockContactList.first() }
            .assertNoErrors()
            .assertComplete()
    }

    @Test
    fun `getContactById with flowable of various contacts as input and id not present`() {
        every { mockRepository.contacts() } returns Flowable.just(mockContactList)

        useCase.getContactById(5).test()
            .assertError(NoSuchElementException::class.java)
            .assertFailure(NoSuchElementException::class.java)
    }


}