package org.loriz.domain.usecases

import io.reactivex.Flowable
import io.reactivex.Single
import org.loriz.data.entities.Contact

interface GetContactsUseCase {
    var chosenContactId: Long
    fun getContacts(): Flowable<List<Contact>>
    fun getContactById(chosenId: Long): Single<Contact>
}