package org.loriz.domain.usecases

import io.reactivex.Flowable
import io.reactivex.Single
import org.loriz.data.entities.Contact
import org.loriz.data.repository.ContactRepository

class GetContactsUseCaseImpl(private val contactsRepository: ContactRepository) : GetContactsUseCase {

    override var chosenContactId: Long = 0L


    override fun getContactById(chosenId: Long): Single<Contact> =
        getContacts().map { contactList ->
            contactList.find { it.id == chosenId } ?: throw NoSuchElementException()
        }.firstOrError()


    override fun getContacts(): Flowable<List<Contact>> {
        return contactsRepository.contacts().map { contactList ->
            val result: HashMap<Long, Contact> = hashMapOf()

            contactList.forEach { contactToMap ->
                val contact = result.getOrPut(contactToMap.id) { contactToMap }
                contact.emails.addAll(contactToMap.emails)
                contact.phoneNumbers.addAll(contactToMap.phoneNumbers)
            }

            result.values.toList().sortedBy { it.displayName }
        }
    }

}