package org.loriz.contactdemo

import junit.framework.TestCase.assertFalse
import org.junit.Test
import org.koin.standalone.inject
import org.koin.test.KoinTest
import org.loriz.data.datasource.ContactDataSource

//run on device with contacts
class ContactsDatasourceTest : KoinTest {


    private val contactsDatasource: ContactDataSource by inject()

    @Test
    fun test_fetch_real_contacts_using_datasource() {
        contactsDatasource.fetchContacts().blockingForEach {
            it.forEach {
                assertFalse(it.displayName.isNullOrBlank())
            }
        }
    }


}