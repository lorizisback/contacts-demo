package org.loriz.contactdemo

import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.loriz.contactdemo.extensions.toEmailUri
import org.loriz.contactdemo.extensions.toPhoneUri
import org.robolectric.RobolectricTestRunner

@RunWith( RobolectricTestRunner::class)
class StringExtensionsTest {


    @Test
    fun toPhoneUri() {
        assertEquals("tel:3923547565", "3923547565".toPhoneUri().toString())
        assertEquals("tel:3923547565", "3923547565 ".toPhoneUri().toString())
        assertEquals("tel:3923547565", "3923547565 -".toPhoneUri().toString())
    }

    @Test
    fun toEmailUri() {
        assertEquals("mail:marco@ciao.com", "marco@ciao.com".toEmailUri().toString())
        assertEquals("mail:marco@ciao.com", "marco@ciao.com ".toEmailUri().toString())
    }
}