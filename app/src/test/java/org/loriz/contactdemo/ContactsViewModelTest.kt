package org.loriz.contactdemo

import io.mockk.*
import io.reactivex.Flowable
import io.reactivex.Single
import junit.framework.TestCase.assertEquals
import org.junit.Before
import org.junit.Test
import org.loriz.contactdemo.ui.view.ContactsViewModel
import org.loriz.data.entities.Contact
import org.loriz.domain.usecases.GetContactsUseCase

class ContactsViewModelTest {

    private val useCase = mockk<GetContactsUseCase>()
    private val mockRepositoryGetContactsAnswer = mockk<Flowable<List<Contact>>>()
    private val mockRepositoryGetContactByIdAnswer = mockk<Single<Contact>>()

    private val viewModel = ContactsViewModel(useCase)
    private val sampleId = 10L


    @Before
    fun warmup() {
        every { useCase.chosenContactId = any() } just Runs
        every { useCase.chosenContactId } returns sampleId
        every { useCase.getContacts() } returns mockRepositoryGetContactsAnswer
        every { useCase.getContactById(any()) } returns mockRepositoryGetContactByIdAnswer
    }

    @Test
    fun `test setChosenContact`() {
        viewModel.setChosenContact(sampleId)
        assertEquals(sampleId, useCase.chosenContactId)
    }

    @Test
    fun `test getChosenContact`() {
        assertEquals(sampleId, viewModel.getChosenContact())
    }

    @Test
    fun `test getContacts`() {
        viewModel.getContacts()
        verify { useCase.getContacts() }
    }

    @Test
    fun `test getContactById`() {
        viewModel.getContactById(sampleId)
        verify { useCase.getContactById(any()) }
    }

}