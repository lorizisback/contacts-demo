package org.loriz.contactdemo.extensions

import android.net.Uri
import androidx.core.net.toUri

fun String.toPhoneUri() : Uri? = "tel:${this.replace("-","").trim()}".toUri()

fun String.toEmailUri() : Uri? = "mailto:${this.trim()}".toUri()