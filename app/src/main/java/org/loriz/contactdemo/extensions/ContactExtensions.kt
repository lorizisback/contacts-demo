package org.loriz.contactdemo.extensions

import androidx.core.net.toUri
import net.gotev.recycleradapter.AdapterItem
import org.loriz.contactdemo.ui.uielements.UiContactListItemElement
import org.loriz.data.entities.Contact

fun Contact.toUiElement(callback: (Long) -> Unit): AdapterItem<*> =
    UiContactListItemElement(id, displayName, thumbnail?.toUri(), callback)
