package org.loriz.contactdemo.extensions

import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable

// the drive extension function mimicks iOS' RxSwift drive which subscribes on the main thread and subscribes passed function

fun <T> Single<T>.drive(onSuccess: (T) -> Unit, onError: (Throwable) -> Unit): Disposable {
    return observeOn(AndroidSchedulers.mainThread()).subscribe(onSuccess, onError)
}

fun <T> Flowable<T>.drive(
    onSuccess: (T) -> Unit,
    onError: (Throwable) -> Unit,
    onComplete: (() -> Unit)? = null
): Disposable {
    return if (onComplete != null) {
        observeOn(AndroidSchedulers.mainThread()).subscribe(onSuccess, onError, { onComplete() })
    } else {
        observeOn(AndroidSchedulers.mainThread()).subscribe(onSuccess, onError)
    }
}
