package org.loriz.contactdemo.extensions

import android.content.Context
import android.graphics.drawable.Drawable
import android.net.Uri
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import org.loriz.contactdemo.R

fun Context.createRoundedDrawable(uri: Uri): Drawable? {
    val inputStream = contentResolver.openInputStream(uri)
    val circularBitmapDrawable = RoundedBitmapDrawableFactory.create(resources, inputStream ?: return null)
    circularBitmapDrawable.isCircular = true
    return circularBitmapDrawable
}

fun Context.getRandomMaterialColor(): Int {
    val colors = resources.obtainTypedArray(R.array.materialColors)
    return colors.getColor((0 until colors.length()).random(), -1).also { colors.recycle() }
}

