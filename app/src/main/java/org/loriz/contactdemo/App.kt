package org.loriz.contactdemo

import android.app.Application
import org.koin.android.ext.android.startKoin
import org.loriz.contactdemo.di.dataModule
import org.loriz.contactdemo.di.domainModule
import org.loriz.contactdemo.di.presentationModule

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin(
            this,
            listOf(
                dataModule,
                domainModule,
                presentationModule
            )
        )
    }

}