package org.loriz.contactdemo.di

import com.squareup.sqlbrite3.SqlBrite
import net.gotev.recycleradapter.RecyclerAdapter
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import org.loriz.contactdemo.ui.view.ContactsViewModel
import org.loriz.data.datasource.ContactDataSource
import org.loriz.data.datasource.ContactDataSourceImpl
import org.loriz.data.repository.ContactRepository
import org.loriz.data.repository.ContactRepositoryImpl
import org.loriz.domain.usecases.GetContactsUseCase
import org.loriz.domain.usecases.GetContactsUseCaseImpl

val dataModule = module {
    single { SqlBrite.Builder().build() }
    single<ContactDataSource> { ContactDataSourceImpl(androidApplication().contentResolver, get()) }
    single<ContactRepository> { ContactRepositoryImpl(get()) }
}

val domainModule = module {
    single<GetContactsUseCase> { GetContactsUseCaseImpl(get()) }
}

val presentationModule = module {
    viewModel { ContactsViewModel(get()) }
    factory { RecyclerAdapter() }
}


