package org.loriz.contactdemo.ui.view

import android.os.Bundle
import org.loriz.contactdemo.R
import org.loriz.contactdemo.ui.base.BaseActivity

class ContactActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contacts)
    }

}