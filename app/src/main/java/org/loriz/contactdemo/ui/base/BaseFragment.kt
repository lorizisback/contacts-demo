package org.loriz.contactdemo.ui.base

import androidx.fragment.app.Fragment
import io.reactivex.disposables.CompositeDisposable

open class BaseFragment : Fragment(), AutoDisposableInterface {

    override val disposableBag: CompositeDisposable = CompositeDisposable()

    override fun onDestroy() {
        super.onDestroy()
        disposableBag.clear()
    }

}