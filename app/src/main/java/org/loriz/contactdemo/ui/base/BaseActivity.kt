package org.loriz.contactdemo.ui.base

import androidx.appcompat.app.AppCompatActivity
import io.reactivex.disposables.CompositeDisposable

open class BaseActivity : AppCompatActivity(), AutoDisposableInterface {

    override val disposableBag: CompositeDisposable = CompositeDisposable()

    override fun onDestroy() {
        disposableBag.clear()
        super.onDestroy()
    }
}