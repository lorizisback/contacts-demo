package org.loriz.contactdemo.ui.uielements

import android.view.View
import kotlinx.android.extensions.LayoutContainer
import net.gotev.recycleradapter.AdapterItem
import net.gotev.recycleradapter.RecyclerAdapterViewHolder
import org.loriz.contactdemo.R

open class UiNoPermissionElement : AdapterItem<UiNoPermissionElement.Holder>() {

    override fun diffingId() = javaClass.name

    override fun getLayoutId() = R.layout.uielement_no_permission

    override fun bind(holder: UiNoPermissionElement.Holder) {}


    class Holder(itemView: View) : RecyclerAdapterViewHolder(itemView), LayoutContainer {

        override val containerView: View?
            get() = itemView

    }

}