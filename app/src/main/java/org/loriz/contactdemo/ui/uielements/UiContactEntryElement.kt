package org.loriz.contactdemo.ui.uielements

import android.graphics.drawable.Drawable
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.uielement_detail_info_entry.*
import net.gotev.recycleradapter.AdapterItem
import net.gotev.recycleradapter.RecyclerAdapterViewHolder
import org.loriz.contactdemo.R

open class UiContactEntryElement(
    private val text: String,
    private val icon: Drawable?,
    private val action: (() -> Unit)? = null
) : AdapterItem<UiContactEntryElement.Holder>() {

    override fun diffingId() = javaClass.name + text

    override fun getLayoutId() = R.layout.uielement_detail_info_entry

    override fun hasToBeReplacedBy(newItem: AdapterItem<*>): Boolean =
        when {
            newItem !is UiContactEntryElement -> true
            text != newItem.text -> true
            else -> false
        }

    override fun bind(holder: UiContactEntryElement.Holder) {
        holder.titleField.text = text
        holder.iconField.setImageDrawable(icon)
        holder.containerView?.setOnClickListener { action?.invoke() }
    }


    class Holder(itemView: View) : RecyclerAdapterViewHolder(itemView), LayoutContainer {

        override val containerView: View?
            get() = itemView

        internal val titleField: AppCompatTextView by lazy { contact_detail_entry_title }
        internal val iconField: AppCompatImageView by lazy { contact_detail_entry_icon }

    }

}