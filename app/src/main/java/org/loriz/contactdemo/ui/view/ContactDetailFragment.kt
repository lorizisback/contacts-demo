package org.loriz.contactdemo.ui.view

import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_contact_detail.*
import net.gotev.recycleradapter.RecyclerAdapter
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.loriz.contactdemo.R
import org.loriz.contactdemo.extensions.drive
import org.loriz.contactdemo.extensions.toEmailUri
import org.loriz.contactdemo.extensions.toPhoneUri
import org.loriz.contactdemo.ui.base.BaseFragment
import org.loriz.contactdemo.ui.uielements.UiContactEntryElement
import org.loriz.contactdemo.ui.uielements.UiContactHeaderElement
import org.loriz.data.entities.Contact


class ContactDetailFragment : BaseFragment() {


    private val viewModel: ContactsViewModel by sharedViewModel()
    private val recyclerAdapter: RecyclerAdapter by inject()

    private val phoneDrawable: Drawable? by lazy {
        ContextCompat.getDrawable(
            requireContext(),
            R.drawable.ic_phone_action
        )
    }

    private val emailDrawable: Drawable? by lazy {
        ContextCompat.getDrawable(
            requireContext(),
            R.drawable.ic_email_action
        )
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_contact_detail, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.setNavigationOnClickListener {
            requireActivity().supportFragmentManager.popBackStack()
        }

        viewModel.getContactById(viewModel.getChosenContact())
            .drive({
                setupView(it)
            }, {
                it.printStackTrace()
            })
    }


    private fun setupView(contact: Contact) {

        contact.displayName?.let {
            collapsing_toolbar_layout.title = it
        }

        contact.photo?.let { photo ->
            toolbar_imageview.setImageURI(photo.toUri())
        }

        recycler_view.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = recyclerAdapter
        }

        // if contact contains phoneumber, prepare according views and add it to recycleradapter
        contact.phoneNumbers.takeIf { it.isNotEmpty() }?.let { phoneNumbersSet ->
            recyclerAdapter.add(UiContactHeaderElement(getString(R.string.contacts_header)))

            phoneNumbersSet.forEach { phoneNumber ->
                recyclerAdapter.add(UiContactEntryElement(phoneNumber, phoneDrawable) {
                    val callIntent = Intent(Intent.ACTION_DIAL).apply {
                        data = phoneNumber.toPhoneUri()
                    }
                    startActivity(callIntent)
                })
            }
        }

        // if contact contains emails, prepare according views and add it to recycleradapter
        contact.emails.takeIf { it.isNotEmpty() }?.let { emailSet ->
            recyclerAdapter.add(UiContactHeaderElement(getString(R.string.emails_header)))

            emailSet.forEach { email ->
                recyclerAdapter.add(UiContactEntryElement(email, emailDrawable) {
                    val emailIntent = Intent(Intent.ACTION_SENDTO).apply {
                        type = "text/plain"
                        data = email.toEmailUri()
                    }
                    startActivity(emailIntent)
                })
            }
        }

    }
}