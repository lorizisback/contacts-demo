package org.loriz.contactdemo.ui.base

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

// this makes autoDispose extension function available in all classes extending this interface
interface AutoDisposableInterface {

    val disposableBag: CompositeDisposable

    fun Disposable.autoDispose() {
        disposableBag.add(this)
    }

}