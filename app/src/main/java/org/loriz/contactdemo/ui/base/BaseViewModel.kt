package org.loriz.contactdemo.ui.base

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

open class BaseViewModel : ViewModel(), AutoDisposableInterface {

    override val disposableBag = CompositeDisposable()

    override fun onCleared() {
        disposableBag.clear()
    }
}