package org.loriz.contactdemo.ui.uielements

import android.content.res.ColorStateList
import android.net.Uri
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.view.ViewCompat
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.uielement_list_entry.*
import net.gotev.recycleradapter.AdapterItem
import net.gotev.recycleradapter.RecyclerAdapterViewHolder
import org.loriz.contactdemo.R
import org.loriz.contactdemo.extensions.createRoundedDrawable
import org.loriz.contactdemo.extensions.getRandomMaterialColor

open class UiContactListItemElement(
    private val id: Long,
    private val name: String?,
    private val thumbnail: Uri? = null,
    private val action: ((Long) -> Unit)
) : AdapterItem<UiContactListItemElement.Holder>() {

    override fun diffingId() = javaClass.name + name

    override fun getLayoutId() = R.layout.uielement_list_entry

    override fun hasToBeReplacedBy(newItem: AdapterItem<*>): Boolean =
        when {
            newItem !is UiContactListItemElement -> true
            id != newItem.id -> true
            else -> false
        }

    override fun bind(holder: UiContactListItemElement.Holder) {
        holder.titleField.text = name

        holder.logoField.text = name?.first().toString()
        ViewCompat.setBackgroundTintList(
            holder.logoField,
            ColorStateList.valueOf(holder.logoField.context.getRandomMaterialColor())
        )

        //if contact has no thumbnail, show the logo instead
        thumbnail?.let {
            holder.iconField.apply {
                visibility = View.VISIBLE
                setImageDrawable(holder.iconField.context.createRoundedDrawable(it))
            }
        } ?: { holder.iconField.visibility = View.INVISIBLE }()


        holder.containerView?.setOnClickListener { action.invoke(id) }
    }


    class Holder(itemView: View) : RecyclerAdapterViewHolder(itemView), LayoutContainer {

        override val containerView: View?
            get() = itemView

        internal val titleField: AppCompatTextView by lazy { list_item_title }
        internal val iconField: AppCompatImageView by lazy { list_item_image }
        internal val logoField: AppCompatTextView by lazy { list_item_logo }

    }

}