package org.loriz.contactdemo.ui.view

import io.reactivex.Flowable
import io.reactivex.Single
import org.loriz.contactdemo.ui.base.BaseViewModel
import org.loriz.data.entities.Contact
import org.loriz.domain.usecases.GetContactsUseCase

class ContactsViewModel(private val getContactsUseCase: GetContactsUseCase) : BaseViewModel() {

    fun setChosenContact(contactId: Long) { getContactsUseCase.chosenContactId = contactId }
    fun getChosenContact() : Long = getContactsUseCase.chosenContactId


    fun getContacts(): Flowable<List<Contact>> = getContactsUseCase.getContacts()
    fun getContactById(id: Long): Single<Contact> = getContactsUseCase.getContactById(id)

}