package org.loriz.contactdemo.ui.uielements

import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.uielement_detail_info_header.*
import net.gotev.recycleradapter.AdapterItem
import net.gotev.recycleradapter.RecyclerAdapterViewHolder
import org.loriz.contactdemo.R

open class UiContactHeaderElement(private val text: String) : AdapterItem<UiContactHeaderElement.Holder>() {

    override fun diffingId() = javaClass.name + text

    override fun getLayoutId() = R.layout.uielement_detail_info_header

    override fun bind(holder: UiContactHeaderElement.Holder) {
        holder.titleField.text = text
    }


    class Holder(itemView: View) : RecyclerAdapterViewHolder(itemView), LayoutContainer {

        override val containerView: View?
            get() = itemView

        internal val titleField: AppCompatTextView by lazy { contact_detail_header_title }

    }

}