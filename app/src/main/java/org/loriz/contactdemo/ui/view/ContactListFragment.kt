package org.loriz.contactdemo.ui.view

import android.Manifest
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.karumi.dexter.Dexter
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.single.BasePermissionListener
import kotlinx.android.synthetic.main.fragment_contact_list.*
import net.gotev.recycleradapter.AdapterItem
import net.gotev.recycleradapter.RecyclerAdapter
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.loriz.contactdemo.R
import org.loriz.contactdemo.extensions.drive
import org.loriz.contactdemo.extensions.toUiElement
import org.loriz.contactdemo.ui.base.BaseFragment
import org.loriz.contactdemo.ui.uielements.UiLoadingElement
import org.loriz.contactdemo.ui.uielements.UiNoItemsElement
import org.loriz.contactdemo.ui.uielements.UiNoPermissionElement
import java.util.*
import java.util.concurrent.TimeUnit


class ContactListFragment : BaseFragment() {

    private val viewModel: ContactsViewModel by sharedViewModel()
    private val recyclerAdapter: RecyclerAdapter by inject()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_contact_list, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Dexter.withActivity(requireActivity())
            .withPermission(Manifest.permission.READ_CONTACTS)
            .withListener(object : BasePermissionListener() {
                override fun onPermissionGranted(response: PermissionGrantedResponse) {
                    setupRecyclerView()
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse) {
                    recycler_view.apply {
                        layoutManager = LinearLayoutManager(requireContext())
                        adapter = recyclerAdapter
                        recyclerAdapter.setEmptyItem(UiNoPermissionElement())
                    }
                }
            }).check()

    }


    private fun setupRecyclerView() {

        recycler_view.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = recyclerAdapter
            recyclerAdapter.setEmptyItem(UiLoadingElement())
        }

        // subscribes to the contacts observable (delay for aesthetic reasons)
        viewModel.getContacts().delay(500L, TimeUnit.MILLISECONDS).map {

            // maps list of contacts to list of uielements
            it.map { contact ->
                contact.toUiElement { id ->
                    viewModel.setChosenContact(id)
                    val action =
                        ContactListFragmentDirections
                            .actionContactListFragmentToContactDetailFragment()
                    findNavController().navigate(action)
                }
            }

        }.drive({ listAdapterItem ->

            recyclerAdapter.apply {
                if (listAdapterItem.isEmpty()) {
                    setEmptyItem(UiNoItemsElement())
                } else {
                    syncWithItems(listAdapterItem as ArrayList<out AdapterItem<*>>)
                }
            }

        }, {
            it.printStackTrace()
        }).autoDispose()
    }


}