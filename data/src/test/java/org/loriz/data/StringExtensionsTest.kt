package org.loriz.data

import org.junit.Test
import org.loriz.data.common.removeDashes
import org.loriz.data.common.removeWhitespaces
import kotlin.test.assertEquals

class StringExtensionsTest {


    @Test
    fun removeWhitespaces() {
        assertEquals("ciao", "ciao ".removeWhitespaces())
        assertEquals("ciao", "ciao".removeWhitespaces())
        assertEquals("", "    ".removeWhitespaces())
    }

    @Test
    fun removeDashes() {
        assertEquals("ciao", "ciao-".removeDashes())
        assertEquals("ciao", "ciao".removeDashes())
        assertEquals("", "----".removeDashes())
    }
}