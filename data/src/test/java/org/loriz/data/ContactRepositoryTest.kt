package org.loriz.data

import io.mockk.every
import io.mockk.mockk
import io.reactivex.Flowable
import junit.framework.TestCase.assertEquals
import org.junit.Test
import org.loriz.data.datasource.ContactDataSource
import org.loriz.data.entities.Contact
import org.loriz.data.repository.ContactRepositoryImpl


class ContactRepositoryTest {

    private val contacts = listOf(
        Contact(
            id = 1,
            displayName = "Marco Loriga",
            emails = hashSetOf("marco.loriga.ca@gmail.com", "marco.loriga@it.ey.com"),
            phoneNumbers = hashSetOf("3773240338", "3240987876"),
            thumbnail = "http://www.fake.org",
            photo = "http://www.fake.org"

        ),
        Contact(
            id = 2,
            displayName = "Claudio Loriga",
            emails = hashSetOf("claudio.loriga.ca@gmail.com", "claudio.loriga@it.ey.com"),
            phoneNumbers = hashSetOf("8475893783", "2309403946"),
            thumbnail = "http://www.fake.org",
            photo = "http://www.fake.org"
        ),
        Contact(
            id = 3,
            displayName = "Nenno Loriga",
            emails = hashSetOf(),
            phoneNumbers = hashSetOf(),
            thumbnail = "http://www.fake.org",
            photo = "http://www.fake.org"
        )
    )


    private val datasource = mockk<ContactDataSource>()
    private val repository = ContactRepositoryImpl(datasource)


    @Test
    fun `test repository filters contacts without emails or phone numbers`() {
        every { datasource.fetchContacts() } returns Flowable.just(contacts)

        assertEquals(2, repository.contacts().blockingSingle().count())
        assertEquals(contacts.subList(0,2), repository.contacts().blockingSingle())
    }


    @Test
    fun `test repository propagates errors`() {
        every { datasource.fetchContacts() } returns Flowable.error { SecurityException() }

        repository.contacts()
            .test()
            .assertError(SecurityException::class.java)
    }
}
