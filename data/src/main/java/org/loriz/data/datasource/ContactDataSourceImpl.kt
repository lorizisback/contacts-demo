package org.loriz.data.datasource

import android.content.ContentResolver
import android.database.Cursor
import android.provider.ContactsContract
import com.squareup.sqlbrite3.SqlBrite
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import org.loriz.data.common.removeDashes
import org.loriz.data.common.removeWhitespaces
import org.loriz.data.entities.ColumnIndexes
import org.loriz.data.entities.Contact


internal typealias Data = ContactsContract.Data

internal val projectionParameters = arrayOf(
    Data.CONTACT_ID,
    Data.DISPLAY_NAME_PRIMARY,
    Data.PHOTO_URI,
    Data.PHOTO_THUMBNAIL_URI,
    Data.DATA1,
    Data.MIMETYPE
)

class ContactDataSourceImpl(resolver: ContentResolver, sqlBrite: SqlBrite) : ContactDataSource {

    private val briteResolver = sqlBrite.wrapContentProvider(resolver, Schedulers.io())
    private lateinit var columnIndexes: ColumnIndexes

    override fun fetchContacts(): Flowable<List<Contact>> =
        briteResolver.createQuery(
            Data.CONTENT_URI,
            projectionParameters,
            null,
            null,
            Data.CONTACT_ID,
            true
        ).mapToList {
            with(it) {
                columnIndexes = ColumnIndexes(
                    id = getColumnIndex(Data.CONTACT_ID),
                    displayName = getColumnIndex(Data.DISPLAY_NAME_PRIMARY),
                    photo = getColumnIndex(Data.PHOTO_URI),
                    thumbnail = getColumnIndex(Data.PHOTO_THUMBNAIL_URI),
                    mimetype = getColumnIndex(Data.MIMETYPE),
                    data = getColumnIndex(Data.DATA1)
                )
                fetchContactFromCursor(this)
            }
        }.toFlowable(BackpressureStrategy.LATEST)


    private fun fetchContactFromCursor(cursor: Cursor): Contact =
        with(cursor) {
            val id = getLong(columnIndexes.id)
            val contact =
                Contact(id).apply {
                    displayName = getString(columnIndexes.displayName).takeUnless { it.isNullOrBlank() }
                    photo = getString(columnIndexes.photo)
                    thumbnail = getString(columnIndexes.thumbnail)
                    when (getString(columnIndexes.mimetype)) {
                        ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE -> {
                            emails.add(getString(columnIndexes.data))
                        }
                        ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE -> {
                            phoneNumbers.add(
                                getString(columnIndexes.data)
                                    .removeWhitespaces()
                                    .removeDashes()
                            )
                        }
                    }
                }
            contact
        }


}