package org.loriz.data.datasource

import io.reactivex.Flowable
import org.loriz.data.entities.Contact


interface ContactDataSource {
    fun fetchContacts(): Flowable<List<Contact>>
}