package org.loriz.data.entities

data class ColumnIndexes(val id : Int,
                         val displayName : Int,
                         val photo : Int,
                         val thumbnail : Int,
                         val mimetype : Int,
                         val data: Int)