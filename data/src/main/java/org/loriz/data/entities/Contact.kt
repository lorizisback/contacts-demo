package org.loriz.data.entities

data class Contact(
    val id: Long,
    var displayName: String? = null,
    var photo: String? = null,
    var thumbnail: String? = null,
    var emails: HashSet<String> = hashSetOf(),
    var phoneNumbers: HashSet<String> = hashSetOf()
) : Comparable<Contact> {

    override fun compareTo(other: Contact): Int {
        return displayName?.compareTo(other.displayName ?: return -1) ?: -1
    }

}