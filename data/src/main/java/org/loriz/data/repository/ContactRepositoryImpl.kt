package org.loriz.data.repository

import io.reactivex.Flowable
import org.loriz.data.datasource.ContactDataSource
import org.loriz.data.entities.Contact


class ContactRepositoryImpl(private val contactDataSource: ContactDataSource) : ContactRepository {

    override fun contacts(): Flowable<List<Contact>> =
        contactDataSource.fetchContacts()
            .map {
                it.filter { it.emails.isNotEmpty() || it.phoneNumbers.isNotEmpty() }
            }

}