package org.loriz.data.repository

import io.reactivex.Flowable
import org.loriz.data.entities.Contact


interface ContactRepository {
    fun contacts(): Flowable<List<Contact>>
}