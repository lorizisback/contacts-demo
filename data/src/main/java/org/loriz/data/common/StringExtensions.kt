package org.loriz.data.common

fun String.removeWhitespaces() : String = this.replace("\\s+".toRegex(), "")

fun String.removeDashes() : String = this.replace("-", "")
